/**
 * Created by Zheka on 25.02.2016.
 */
public class SimpleSmallestMultipliers {

    public static void main(String[] args) {
        int number = 18;
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                number = number / i;
                System.out.println(i);
                if (i != 1) {
                    i--;
                }
            }
        }
    }
}